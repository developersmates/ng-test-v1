import { NgTestV1Page } from './app.po';

describe('ng-test-v1 App', () => {
  let page: NgTestV1Page;

  beforeEach(() => {
    page = new NgTestV1Page();
  });

  it('should display welcome message', done => {
    page.navigateTo();
    page.getParagraphText()
      .then(msg => expect(msg).toEqual('Welcome to app!!'))
      .then(done, done.fail);
  });
});
